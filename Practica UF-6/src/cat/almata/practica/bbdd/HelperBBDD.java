package cat.almata.practica.bbdd;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;


import cat.almata.practica.domini.Alumne;
import cat.almata.practica.domini.Professor;
import cat.almata.practica.excepcions.gestorDBexception;

public class HelperBBDD {
	
	
	public static void addAlumne(Alumne alumne) throws gestorDBexception {
		String comanda="INSERT INTO alumnes(dni, nomUsuari, dataNaixement, descripcio, "
		+ "preuMatricula, esqui, natacio, escalada, equitacio, senderisme, dniProfessor) "
		+ "VALUES ('"+alumne.getDni()+"','"+alumne.getNomUsuari()+"','"+alumne.getDataNaixement()+"',"
		+ "'"+alumne.getDescripcio()+"',"+alumne.getPreuMatricula()+","+alumne.isEsqui()+","+alumne.isNatacio()+","
		+ ""+alumne.isEscalada()+","+alumne.isEquitacio()+","+alumne.isSenderisme()+",'"+alumne.getProfessor().getDni()+"')";
		GestorDB.modificaDB(comanda);
	}
	public static Alumne getAlumne(String keyDni) throws gestorDBexception{
		String comanda="SELECT * FROM alumnes WHERE dni='"+keyDni+"'";
		List<Alumne> alumne=GestorDB.consultaDB(comanda, Alumne.class);

			return alumne.get(0);

			
		
	}
	public static Map<String, Alumne> getAllAlumnes() throws gestorDBexception{
		String comanda = "SELECT * FROM alumnes";
		List<Alumne> llistaAlumnes=GestorDB.consultaDB(comanda, Alumne.class);
		Map<String, Alumne> mapaAlumnes = new TreeMap<String, Alumne>();
		if(llistaAlumnes!=null) {
			for(Alumne alumne:llistaAlumnes) {
				mapaAlumnes.put(alumne.getDni(), alumne);
			}
		}
		return mapaAlumnes;
	}
	public static void removeAlumne(String keyDNI) throws gestorDBexception {
		String comanda="DELETE FROM alumnes WHERE dni='"+keyDNI+"'";
		GestorDB.modificaDB(comanda);
	}
	
	
	
	
	
	public static void addProfessor(Professor professor) throws gestorDBexception {
		String comanda="INSERT INTO professors(dni, nomUsuari, nom, cognoms, dataNaixement, poblacio, codiPostal, oposicions) "
				+ "VALUES ('"+professor.getDni()+"','"+professor.getNomUsuari()+"','"+professor.getNom()+"','"+professor.getCognoms()+"',"
						+ "'"+professor.getDataNaixement()+"','"+professor.getPoblacio()+"',"+professor.getCodiPostal()+","+professor.isOposicions()+")";
		
		GestorDB.modificaDB(comanda);
	}
	public static Professor getProfessor(String keyDni) throws gestorDBexception {
		String comanda="SELECT * FROM professors WHERE dni='"+keyDni+"'";
		List<Professor> professor=GestorDB.consultaDB(comanda, Professor.class);
		
		
			return professor.get(0);
		
	}
	public static Map<String, Professor> getAllProfessors() throws gestorDBexception{
		String comanda = "SELECT * FROM professors";
		List<Professor> llistaProfessors=GestorDB.consultaDB(comanda, Professor.class);
		Map<String, Professor> mapaProfessors = new TreeMap<String, Professor>();
		if(llistaProfessors!=null) {
			for(Professor professor:llistaProfessors) {
				mapaProfessors.put(professor.getDni(), professor);
			}
		}
		return mapaProfessors;
	}
	public static void removeProfessor(String keyDNI) throws gestorDBexception{
		String comanda="DELETE FROM professors WHERE dni='"+keyDNI+"'";
		GestorDB.modificaDB(comanda);
	}
	

}

package cat.almata.practica.bbdd;

public class Configuracio {
	public static final String PROTOCOL="jdbc:mysql://";
	public static final String IP_SERVIDOR="127.0.0.1";
	public static final String PORT_BBDD="3306";
	public static final String BBDD="prova";
	public static final String USUARI="ifiguera";
	public static final String PASSWORD="ifiguera";
	public static final String DRIVER_NAME="com.mysql.cj.jdbc.Driver";
	
}

package cat.almata.practica;
import java.util.ArrayList;

import cat.almata.practica.domini.Alumne;


public class LlistaAlumnes {
	private ArrayList<Alumne> alumnes;

	public LlistaAlumnes(ArrayList<Alumne> alumnes) {
		super();
		this.alumnes = alumnes;
	}

	public ArrayList<Alumne> getAlumnes() {
		return alumnes;
	}

	public void setAlumnes(ArrayList<Alumne> alumnes) {
		this.alumnes = alumnes;
	}

	@Override
	public String toString() {
		return "LlistaAlumnes [alumnes=" + alumnes + "]";
	}


	
	

}

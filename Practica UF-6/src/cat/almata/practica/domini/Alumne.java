package cat.almata.practica.domini;


import java.util.Objects;

public class Alumne {
	private String dni;
	private String nomUsuari;
	private String dataNaixement;
	private String descripcio;
	private double preuMatricula;
	private boolean esqui;
	private boolean natacio;
	private boolean escalada;
	private boolean equitacio;
	private boolean senderisme;
	private Professor professor;
	private String dniProfessor;
	
	



	public String getDniProfessor() {
		return dniProfessor;
	}





	public Alumne(String dni, String nomUsuari, String dataNaixement, String descripcio, double preuMatricula,
			boolean esqui, boolean natacio, boolean escalada, boolean equitacio, boolean senderisme,
			Professor professor) {
		super();
		this.dni = dni;
		this.nomUsuari = nomUsuari;
		this.dataNaixement = dataNaixement;
		this.descripcio = descripcio;
		this.preuMatricula = preuMatricula;
		this.esqui = esqui;
		this.natacio = natacio;
		this.escalada = escalada;
		this.equitacio = equitacio;
		this.senderisme = senderisme;
		this.professor = professor;
		professor.addAlumne(this);
		dniProfessor=professor.getDni();
	}


	


	public Alumne() {
		super();
	}





	public Alumne(String dni) {
		super();
		this.dni = dni;
	}


	public String getDni() {
		return dni;
	}


	public void setDni(String dni) {
		this.dni = dni;
	}


	public String getNomUsuari() {
		return nomUsuari;
	}


	public void setNomUsuari(String nomUsuari) {
		this.nomUsuari = nomUsuari;
	}


	public String getDataNaixement() {
		return dataNaixement;
	}


	public void setDataNaixement(String dataNaixement) {
		this.dataNaixement = dataNaixement;
	}


	public void setDniProfessor(String dniProfessor) {
		this.dniProfessor = dniProfessor;
	}





	public String getDescripcio() {
		return descripcio;
	}


	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}


	public double getPreuMatricula() {
		return preuMatricula;
	}


	public void setPreuMatricula(double preuMatricula) {
		this.preuMatricula = preuMatricula;
	}


	public boolean isEsqui() {
		return esqui;
	}


	public void setEsqui(boolean esqui) {
		this.esqui = esqui;
	}


	public boolean isNatacio() {
		return natacio;
	}


	public void setNatacio(boolean natacio) {
		this.natacio = natacio;
	}


	public boolean isEscalada() {
		return escalada;
	}


	public void setEscalada(boolean escalada) {
		this.escalada = escalada;
	}


	public boolean isEquitacio() {
		return equitacio;
	}


	public void setEquitacio(boolean equitacio) {
		this.equitacio = equitacio;
	}


	public boolean isSenderisme() {
		return senderisme;
	}


	public void setSenderisme(boolean senderisme) {
		this.senderisme = senderisme;
	}


	public Professor getProfessor() {
		return professor;
	}


	public void setProfessor(Professor professor) {
		this.professor = professor;
	}


	@Override
	public String toString() {
		return "Alumne [dni=" + dni + ", nomUsuari=" + nomUsuari + ", dataNaixement=" + dataNaixement + ", descripcio="
				+ descripcio + ", preuMatricula=" + preuMatricula + ", esqui=" + esqui + ", natacio=" + natacio
				+ ", escalada=" + escalada + ", equitacio=" + equitacio + ", senderisme=" + senderisme + ", professor="
				+ professor + "]";
	}


	@Override
	public int hashCode() {
		return Objects.hash(dni);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Alumne other = (Alumne) obj;
		return Objects.equals(dni, other.dni);
	}



	





	
	
	
	
}

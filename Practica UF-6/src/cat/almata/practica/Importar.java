package cat.almata.practica;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import cat.almata.practica.domini.Alumne;
import cat.almata.practica.domini.Professor;
import cat.almata.practica.excepcions.gestorDBexception;
import cat.almata.practica.presentacio.Controlador_de_Presentacio;

public class Importar {
	public static void fer() {
		
		
		
		
		BufferedReader archivoJsonBufferedReader=null;
		BufferedReader archivoJsonBufferedReader2=null;
		try {
			archivoJsonBufferedReader = new BufferedReader(new FileReader("C:\\Users\\alum-01\\Desktop\\Programacio\\UF-6\\projectes\\Practica UF-6\\JSON\\Alumnes.json"));
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			archivoJsonBufferedReader2 = new BufferedReader(new FileReader("C:\\Users\\alum-01\\Desktop\\Programacio\\UF-6\\projectes\\Practica UF-6\\JSON\\Professors.json"));
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		LlistaAlumnes llA= (LlistaAlumnes) JSONTools.JSONStreamToDataModel(archivoJsonBufferedReader, LlistaAlumnes.class);
		LlistaProfessors llP= (LlistaProfessors) JSONTools.JSONStreamToDataModel(archivoJsonBufferedReader2, LlistaProfessors.class);
		
		ArrayList<Alumne> alA = new ArrayList<>(llA.getAlumnes());
		ArrayList<Professor> alP = new ArrayList<>(llP.getProfessors());
		
		try {
			archivoJsonBufferedReader.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			archivoJsonBufferedReader2.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		for(Professor p:alP) {
			try {
				Controlador_de_Presentacio.removeProfessor(p.getDni());
				Controlador_de_Presentacio.addProfessor(p.getDni(),
						p.getNomUsuari(),
						p.getNom(),
						p.getCognoms(),
						p.getDataNaixement(),
						p.getPoblacio(),
						Integer.valueOf(p.getCodiPostal()),
						p.isOposicions());
			} catch (gestorDBexception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		for(Alumne a:alA) {
			Professor p1=null;
			Map<String, Professor> professorsTOTS=null;
			try {
				professorsTOTS = Controlador_de_Presentacio.getAllProfessors();
			} catch (gestorDBexception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			for(Professor p : professorsTOTS.values()) {
				if(a.getDniProfessor().equals(p.getDni())) {
					p1=p;
				}
			}
			try {
				Controlador_de_Presentacio.removeAlumne(a.getDni());
				Controlador_de_Presentacio.addAlumne(a.getDni(), a.getNomUsuari(), a.getDataNaixement(),
						a.getDescripcio(), a.getPreuMatricula(), a.isEsqui(), a.isNatacio(), a.isEscalada(),
						a.isEquitacio(), a.isSenderisme(), p1);
			} catch (gestorDBexception e) {
				
			}
		}
		
		

	}
}

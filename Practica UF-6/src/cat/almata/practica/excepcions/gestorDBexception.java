package cat.almata.practica.excepcions;

public class gestorDBexception extends Exception {
	
	private static final long serialVersionUID = 1L;

	public gestorDBexception(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public gestorDBexception(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public gestorDBexception(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

}

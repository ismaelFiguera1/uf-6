package cat.almata.practica;
import java.util.ArrayList;

import cat.almata.practica.domini.Professor;


public class LlistaProfessors {
	private ArrayList<Professor> professors;

	public LlistaProfessors(ArrayList<Professor> professors) {
		super();
		this.professors = professors;
	}

	public ArrayList<Professor> getProfessors() {
		return professors;
	}

	public void setPersones(ArrayList<Professor> professors) {
		this.professors = professors;
	}

	@Override
	public String toString() {
		return "LlistaProfessors [professors=" + professors + "]";
	}

	
	
	

}

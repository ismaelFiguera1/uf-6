package cat.almata.practica;

import java.io.BufferedReader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


public class JSONTools {

	
	//SERIALITZAR
	//Serialitzar dades JSON
	public static String toJSON(Object o){
		 Gson gson = new Gson();        
		 return gson.toJson(o);
	}
	
	
	//DESERIALITZAR
	/**
	 * Convertir una cadena JSON en un objecte de tipus <T>.
	 * @param jsonString - representa la cadena JSON.
	 * @param <T> - representa la classe de l'objecte que volem convertir.
	 * @return un objecte de tipus <T> corresponent a la cadena d'entrada jsonString
	 */
	public static <T> Object JSONStringToDataModel(String jsonString, Class<T> classe) {		
		//necessitem formatar el tipus date per a que sigui parsejable per json
		//en els models de dades amb atributs de tipus date
		Gson gson=null;
		/*
		if (classe==Event.class || classe==EventList.class ){
			gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
		}else{
			if (classe==MessageList.class ||classe==Message.class ){
				gson=  new GsonBuilder().setDateFormat("EEE MMM dd hh:mm:ss z yyyy").create();
			}else{
				gson=  new GsonBuilder().create();
			}
		}
		*/
		
		gson=  new GsonBuilder().create();

		T obj=gson.fromJson(jsonString, classe);
		return obj;
	}

	
	/**
	 * Convertir una cadena JSON en un objecte de tipus <T>.
	 * @param jsonString - representa la cadena JSON.
	 * @param <T> - representa la classe de l'objecte que volem convertir.
	 * @return un objecte de tipus <T> corresponent a la cadena d'entrada jsonString
	 */
	public static <T> Object JSONStreamToDataModel(BufferedReader jsonBuffer, Class<T> classe) {		
		//necessitem formatar el tipus date per a que sigui parsejable per json
		//en els models de dades amb atributs de tipus date
		Gson gson=null;
		/*
		if (classe==Event.class || classe==EventList.class ){
			gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
		}else{
			if (classe==MessageList.class ||classe==Message.class ){
				gson=  new GsonBuilder().setDateFormat("EEE MMM dd hh:mm:ss z yyyy").create();
			}else{
				gson=  new GsonBuilder().create();
			}
		}
		*/
		
		gson=  new GsonBuilder().create();

		T obj=gson.fromJson(jsonBuffer, classe);
		return obj;
	}
}

package cat.almata.practica;






import javax.swing.SwingUtilities;


import cat.almata.practica.presentacio.Controlador_de_Presentacio;
import cat.almata.practica.presentacio.FormulariPerDefecte;

public class Ppal {
	public static void main(String[] args) {
	
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				Controlador_de_Presentacio.crearAplicacio();
				Controlador_de_Presentacio.canviFormulari(new FormulariPerDefecte());
			}
		});
	}










}

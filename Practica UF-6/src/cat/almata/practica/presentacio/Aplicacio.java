package cat.almata.practica.presentacio;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import cat.almata.practica.Exportar;
import cat.almata.practica.Importar;
import cat.almata.practica.utils.Util;

public class Aplicacio extends JFrame {
	private static final long serialVersionUID = 1L;
	public static final int With = 400;
	public static final int Height=500;
	
	private Controlador controlador;
	private JMenu menuGestioAlumnes;
	private JMenu menuGestioProfessors;
	private JMenu menuGestioJSON;
	private JMenuItem opcioAltaAlumne;
	private JMenuItem opcioConsultaAlumne;
	private JMenuItem opcioModificacioAlumne;
	private JMenuItem opcioEliminarAlumne;
	private JMenuItem opcio_TOTS_Alumne;
	private JMenuItem opcioAltaProfessor;
	private JMenuItem opcioConsultaProfessor;
	private JMenuItem opcioModificacioProfessor;
	private JMenuItem opcioEliminarProfessor;
	private JMenuItem opcio_TOTS_Professor;
	private JMenuItem opcio_ProfessorAlumnes;
	private JMenuItem opcio_ExportarJSON;
	private JMenuItem opcio_ImportarJSON;
	
	
	public Aplicacio() {
		inicialitzacio();
	}
	
	private void inicialitzacio() {
		setBounds(0, 0, 500, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Aplicacio de entrada i sortida de alumnes i professors");
		Util.centrarFinestra(this);
		/*
		 * Les 2 linees de baix no se que fan.
		 */
		Container c = this.getContentPane();
		((JComponent)c).setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		this.setIconImage(new ImageIcon("imatges/images.jpeg").getImage());
		controlador = new Controlador();
		this.setJMenuBar(crearMenu());
	}
	
	private JMenuBar crearMenu() {
		JMenuBar barraMenu = new JMenuBar();
		menuGestioAlumnes=new JMenu("Alumnes");
		menuGestioProfessors=new JMenu("Professors");
		menuGestioJSON=new JMenu("JSON");
		
		opcioAltaAlumne=new JMenuItem("Alta Alumne");
		opcioAltaAlumne.setMnemonic(KeyEvent.VK_A);
		opcioAltaAlumne.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A,ActionEvent.ALT_MASK));
		opcioAltaAlumne.addActionListener(controlador);
		opcioAltaAlumne.setActionCommand("alta_alumne");
		
		
		
		opcioConsultaAlumne=new JMenuItem("Consulta Alumne");
		opcioConsultaAlumne.setMnemonic(KeyEvent.VK_C);
		opcioConsultaAlumne.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C,ActionEvent.ALT_MASK));
		opcioConsultaAlumne.addActionListener(controlador);
		opcioConsultaAlumne.setActionCommand("consulta_alumne");
		
		
		
		opcioModificacioAlumne=new JMenuItem("Modificacio Alumne");
		opcioModificacioAlumne.setMnemonic(KeyEvent.VK_M);
		opcioModificacioAlumne.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M,ActionEvent.ALT_MASK));
		opcioModificacioAlumne.addActionListener(controlador);
		opcioModificacioAlumne.setActionCommand("modificacio_alumne");
		
		
		
		opcioEliminarAlumne=new JMenuItem("Eliminar Alumne");
		opcioEliminarAlumne.setMnemonic(KeyEvent.VK_E);
		opcioEliminarAlumne.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E,ActionEvent.ALT_MASK));
		opcioEliminarAlumne.addActionListener(controlador);
		opcioEliminarAlumne.setActionCommand("eliminar_alumne");
		
		
		
		opcio_TOTS_Alumne=new JMenuItem("TOTS Alumnes");
		opcio_TOTS_Alumne.setMnemonic(KeyEvent.VK_M);
		opcio_TOTS_Alumne.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M,ActionEvent.ALT_MASK));
		opcio_TOTS_Alumne.addActionListener(controlador);
		opcio_TOTS_Alumne.setActionCommand("tots_alumne");
		
		
		
		opcioAltaProfessor=new JMenuItem("Alta Professor");
		opcioAltaProfessor.setMnemonic(KeyEvent.VK_P);
		opcioAltaProfessor.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P,ActionEvent.ALT_MASK));
		opcioAltaProfessor.addActionListener(controlador);
		opcioAltaProfessor.setActionCommand("alta_professor");
		
		
		
		opcioConsultaProfessor=new JMenuItem("Consulta Professor");
		opcioConsultaProfessor.setMnemonic(KeyEvent.VK_C);
		opcioConsultaProfessor.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C,ActionEvent.ALT_MASK));
		opcioConsultaProfessor.addActionListener(controlador);
		opcioConsultaProfessor.setActionCommand("consulta_professor");
		
		
		
		opcioModificacioProfessor=new JMenuItem("Modificacio Professor");
		opcioModificacioProfessor.setMnemonic(KeyEvent.VK_M);
		opcioModificacioProfessor.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M,ActionEvent.ALT_MASK));
		opcioModificacioProfessor.addActionListener(controlador);
		opcioModificacioProfessor.setActionCommand("modificacio_professor");
		
		
		
		opcioEliminarProfessor=new JMenuItem("Eliminar Professor");
		opcioEliminarProfessor.setMnemonic(KeyEvent.VK_E);
		opcioEliminarProfessor.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E,ActionEvent.ALT_MASK));
		opcioEliminarProfessor.addActionListener(controlador);
		opcioEliminarProfessor.setActionCommand("eliminar_professor");
		
		
		
		opcio_TOTS_Professor=new JMenuItem("TOTS Professors");
		opcio_TOTS_Professor.setMnemonic(KeyEvent.VK_P);
		opcio_TOTS_Professor.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P,ActionEvent.ALT_MASK));
		opcio_TOTS_Professor.addActionListener(controlador);
		opcio_TOTS_Professor.setActionCommand("tots_professor");
		
		
		
		
		
		
		opcio_ProfessorAlumnes=new JMenuItem("Professor/Alumne");
		opcio_ProfessorAlumnes.setMnemonic(KeyEvent.VK_Q);
		opcio_ProfessorAlumnes.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q,ActionEvent.ALT_MASK));
		opcio_ProfessorAlumnes.addActionListener(controlador);
		opcio_ProfessorAlumnes.setActionCommand("pa");
		
		
		
		
		
		
		opcio_ExportarJSON=new JMenuItem("Exportar");
		opcio_ExportarJSON.setMnemonic(KeyEvent.VK_E);
		opcio_ExportarJSON.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E,ActionEvent.ALT_MASK));
		opcio_ExportarJSON.addActionListener(controlador);
		opcio_ExportarJSON.setActionCommand("exportar");
		
		
		
		opcio_ImportarJSON=new JMenuItem("Importar");
		opcio_ImportarJSON.setMnemonic(KeyEvent.VK_I);
		opcio_ImportarJSON.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I,ActionEvent.ALT_MASK));
		opcio_ImportarJSON.addActionListener(controlador);
		opcio_ImportarJSON.setActionCommand("importar");
		
		
		
		menuGestioAlumnes.add(opcioAltaAlumne);
		menuGestioAlumnes.addSeparator();
		menuGestioAlumnes.add(opcioConsultaAlumne);
		menuGestioAlumnes.addSeparator();
		menuGestioAlumnes.add(opcioModificacioAlumne);
		menuGestioAlumnes.addSeparator();
		menuGestioAlumnes.add(opcioEliminarAlumne);
		menuGestioAlumnes.addSeparator();
		menuGestioAlumnes.add(opcio_TOTS_Alumne);
		barraMenu.add(menuGestioAlumnes);
		
		
		
		menuGestioProfessors.add(opcioAltaProfessor);
		menuGestioProfessors.addSeparator();
		menuGestioProfessors.add(opcioConsultaProfessor);
		menuGestioProfessors.addSeparator();
		menuGestioProfessors.add(opcioModificacioProfessor);
		menuGestioProfessors.addSeparator();
		menuGestioProfessors.add(opcioEliminarProfessor);
		menuGestioProfessors.addSeparator();
		menuGestioProfessors.add(opcio_TOTS_Professor);
		menuGestioProfessors.addSeparator();
		menuGestioProfessors.add(opcio_ProfessorAlumnes);
		barraMenu.add(menuGestioProfessors);
		
		
		
		menuGestioJSON.add(opcio_ExportarJSON);
		menuGestioJSON.add(opcio_ImportarJSON);
		barraMenu.add(menuGestioJSON);
		
		return barraMenu;
		
	}
	
	private class Controlador implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			Object obj = e.getSource();
			if(obj instanceof JMenuItem) {
				JMenuItem jmi = (JMenuItem) obj;
				if(jmi.getActionCommand().equals("alta_alumne")) {
					Controlador_de_Presentacio.canviFormulari(new FormulariAltaAlumne());	
				}else if(jmi.getActionCommand().equals("alta_professor")){
					Controlador_de_Presentacio.canviFormulari(new FormulariAltaProfessor());
				}else if(jmi.getActionCommand().equals("consulta_alumne")){
					Controlador_de_Presentacio.canviFormulari(new FormulariConsultaAlumne());
				}else if(jmi.getActionCommand().equals("consulta_professor")){
					Controlador_de_Presentacio.canviFormulari(new FormulariConsultaProfessor());
				}else if(jmi.getActionCommand().equals("modificacio_alumne")){
					Controlador_de_Presentacio.canviFormulari(new FormulariModificacioAlumne());
				}else if(jmi.getActionCommand().equals("modificacio_professor")){
					Controlador_de_Presentacio.canviFormulari(new FormulariModificacioProfessor());
				}else if(jmi.getActionCommand().equals("eliminar_alumne")){
					Controlador_de_Presentacio.canviFormulari(new FormulariEliminarAlumne());
				}else if(jmi.getActionCommand().equals("eliminar_professor")){
					Controlador_de_Presentacio.canviFormulari(new FormulariEliminarProfessor());
				}else if(jmi.getActionCommand().equals("tots_alumne")){
					Controlador_de_Presentacio.canviFormulari(new FormulariTOTSalumnes());
				}else if(jmi.getActionCommand().equals("tots_professor")){
					Controlador_de_Presentacio.canviFormulari(new FormulariTOTSprofessors());
				}else if(jmi.getActionCommand().equals("pa")){
					Controlador_de_Presentacio.canviFormulari(new FormulariAlumnes_Porfessors());
				}else if(jmi.getActionCommand().equals("exportar")){
					Exportar.fer();
				}else if(jmi.getActionCommand().equals("importar")){
					Importar.fer();
					
				}
			}
			
		}
		
	}
}

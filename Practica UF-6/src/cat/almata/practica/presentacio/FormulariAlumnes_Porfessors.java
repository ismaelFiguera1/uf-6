package cat.almata.practica.presentacio;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import cat.almata.practica.domini.Alumne;
import cat.almata.practica.domini.Professor;
import cat.almata.practica.excepcions.gestorDBexception;
import cat.almata.practica.utils.Util;

public class FormulariAlumnes_Porfessors extends JInternalFrame implements Formulari {
	private static final long serialVersionUID = 1L;
	
	private GridBagLayout layout;
	private Controlador controlador;
	
	private static int a=0;
	
	private JLabel lblDni;
	private JTextField txtDni;
	private JButton btnCercar;
	private JButton btnSortir;
	private JPanel pnl1;
	
	private JTable taula;
	private JScrollPane jspTaula;
	private DefaultTableModel model;
	
	private Professor professor=null;
	
	public FormulariAlumnes_Porfessors(){
		inicialitzacions();
		crearComponents();
		afegirComponents();
		posicionarComponents();
	}
	
	@Override
	public void inicialitzacions() {
		setTitle("Formulari consulta alumnes del professor");
		getContentPane().setLayout(layout=new GridBagLayout());
		Util.treureBarraTitolInteralFrame(this);
		controlador=new Controlador();
		this.setVisible(true);
	}

	@Override
	public void crearComponents() {
		lblDni=new JLabel("DNI Professor:");
		txtDni=new JTextField();
		
		
		
		btnCercar=new JButton("Cercar");
		btnCercar.addActionListener(controlador);
		btnCercar.setActionCommand("cercar");
		
		btnSortir=new JButton("Sortir");
		btnSortir.addActionListener(controlador);
		btnSortir.setActionCommand("sortir");
		
		pnl1=new JPanel();
		pnl1.setLayout(new GridLayout(1, 2, 5, 0));
		pnl1.add(txtDni);
		pnl1.add(btnCercar);
		
		
		
		model = new DefaultTableModel();
		model.addColumn("DNI");
		model.addColumn("Nom_Usuari");
		model.addColumn("Data de naixement");
		model.addColumn("Preu Matricula");
		
		taula = new JTable(model);
		jspTaula=new JScrollPane(taula);
		
		
	}

	private void omplirTaula() {
		Map<String, Professor> professors=null;
		try {
			professors = Controlador_de_Presentacio.getAllProfessors();
		} catch (gestorDBexception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for(Professor p : professors.values()) {
			if(p.getDni().equals(txtDni.getText())) {
				professor=p;
			}
		}
		
		Map<String, Alumne> alumnes=null;
		try {
			alumnes = professor.getAlumnes(txtDni.getText());
		} catch (gestorDBexception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for(Entry<String,Alumne> objecteMapa: alumnes.entrySet()) {
			Alumne alumne = objecteMapa.getValue();
			Object[] objFila= new Object[4];
			objFila[0]=alumne.getDni();
			objFila[1]=alumne.getNomUsuari();
			objFila[2]=alumne.getDataNaixement();
			objFila[3]=alumne.getPreuMatricula();
			model.addRow(objFila);
		}
	}

	@Override
	public void afegirComponents() {
		// TODO Auto-generated method stub
		add(lblDni);
		add(pnl1);
		add(btnSortir);

		add(jspTaula);
	}

	@Override
	public void posicionarComponents() {
		GridBagConstraints gbc = new GridBagConstraints();

		//lblDni
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=0;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblDni, gbc);

		
		
		//pnlBotons
		gbc.gridx=1;		//	primera columna
		gbc.gridy=0;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=2;		//	factor de creixement
		gbc.weighty=2;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.NONE;
		layout.setConstraints(pnl1, gbc);
		
		
		
		//jspTaula
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=1;		//	primera fila
		gbc.gridheight=5;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=1;		//	factor de creixement
		gbc.weighty=1;		//	factor de creixement
	//	gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.BOTH;
		layout.setConstraints(jspTaula, gbc);
		
		
		
		//btnSortir
		gbc.gridx=1;		//	primera columna
		gbc.gridy=6;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.EAST;
		gbc.fill= GridBagConstraints.NONE;
		layout.setConstraints(btnSortir, gbc);
	}
	
	private class Controlador implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			Object obj = e.getSource();
			if (obj instanceof JButton) {
				JButton boto = (JButton) obj;
				if(boto.getActionCommand().equals("sortir")) {
					Controlador_de_Presentacio.canviFormulari(new FormulariPerDefecte());
				}else if(boto.getActionCommand().equals("cercar")) {
					a++;
					if(a>1) {
						eliminarTaula();
					}
					eliminarTaula();
					omplirTaula();
					txtDni.setText("");
					txtDni.grabFocus();
				}
			}
		
		}

		private void eliminarTaula() {
			int totalFilas = model.getRowCount();
			for(int i=0; i<totalFilas; i++) {
				model.removeRow(0);
			}
				
		
		}

	}
}

package cat.almata.practica.presentacio;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.github.lgooddatepicker.components.DatePicker;

import cat.almata.practica.domini.Professor;
import cat.almata.practica.excepcions.gestorDBexception;
import cat.almata.practica.utils.Fonts;
import cat.almata.practica.utils.Util;





public class FormulariEliminarProfessor extends JInternalFrame implements Formulari {

	private static final long serialVersionUID = 1L;

	private GridBagLayout layout;
	private Controlador controlador;




	private JLabel lblDni;
	private JTextField txtDni;

	private JLabel lblNomUsuari;
	private JTextField txtNomUsuari;

	private JLabel lblNom;
	private JTextField txtNom;

	private JLabel lblCognoms;
	private JTextField txtCognoms;

	private JLabel lblDataNaixement;
	private DatePicker txtDataNaixement;

	private JLabel lblPoblacio;
	private JTextField txtPoblacio;
	
	private JLabel lblCP;
	private JTextField txtCP;
	
	private JCheckBox chkoposicions;

	private JButton btnCercar;
	private JButton btnCancelar;
	private JButton btnEliminar;
	private JPanel pnlBotons;
	
	private FormulariEliminarProfessor fep;





	public FormulariEliminarProfessor() {
		inicialitzacions();
		crearComponents();
		afegirComponents();
		posicionarComponents();
	}

	@Override
	public void inicialitzacions() {
		setTitle("Formulari consulta dades del professor");
		getContentPane().setLayout(layout=new GridBagLayout());
		Util.treureBarraTitolInteralFrame(this);
		controlador=new Controlador();
		this.setVisible(true);
		fep=this;
	}

	@Override
	public void crearComponents() {
		lblDni=new JLabel("DNI:");
		lblDni.setFont(Fonts.fontLabel());

		txtDni=new JTextField(20);
		txtDni.setFont(Fonts.fontTextField());



		lblNomUsuari=new JLabel("Nom de Usuari:");
		lblNomUsuari.setFont(Fonts.fontLabel());

		txtNomUsuari=new JTextField();
		txtNomUsuari.setFont(Fonts.fontTextField());
		txtNomUsuari.setEditable(false);



		lblNom= new JLabel("Nom:");
		lblNom.setFont(Fonts.fontLabel());

		txtNom=new JTextField();
		txtNom.setFont(Fonts.fontTextField());
		txtNom.setEditable(false);



		lblCognoms= new JLabel("Cognoms:");
		lblCognoms.setFont(Fonts.fontLabel());

		txtCognoms=new JTextField();
		txtCognoms.setFont(Fonts.fontTextField());
		txtCognoms.setEditable(false);



		lblDataNaixement=new JLabel("Data de Naixement:");
		lblDataNaixement.setFont(Fonts.fontLabel());

		txtDataNaixement=new DatePicker();
		txtDataNaixement.setEnabled(false);



		lblPoblacio= new JLabel("Poblacio:");
		lblPoblacio.setFont(Fonts.fontLabel());
		
		txtPoblacio= new JTextField();
		txtPoblacio.setFont(Fonts.fontTextField());
		txtPoblacio.setEditable(false);



		lblCP=new JLabel("Codi Postal:");
		lblCP.setFont(Fonts.fontLabel());
		
		txtCP=new JTextField();
		txtCP.setFont(Fonts.fontTextField());
		txtCP.setEditable(false);
		
		
		chkoposicions=new JCheckBox("Oposicions");
		chkoposicions.setEnabled(false);
		
		btnCercar=new JButton("Cercar");
		btnCercar.addActionListener(controlador);
		btnCercar.setActionCommand("cercar");
		btnCancelar=new JButton("Cancelar");
		btnCancelar.addActionListener(controlador);
		btnCancelar.setActionCommand("cancelar");
		btnEliminar=new JButton("Eliminar");
		btnEliminar.addActionListener(controlador);
		btnEliminar.setActionCommand("eliminar");
		
		pnlBotons=new JPanel();
		pnlBotons.setLayout(new GridLayout(1, 2, 5, 0));	
		pnlBotons.add(btnEliminar);
		pnlBotons.add(btnCancelar);


	}

	@Override
	public void afegirComponents() {
		add(lblDni);
		add(txtDni);
		
		add(btnCercar);

		add(lblNomUsuari);
		add(txtNomUsuari);
		
		add(lblNom);
		add(txtNom);
		
		add(lblCognoms);
		add(txtCognoms);

		add(lblDataNaixement);
		add(txtDataNaixement);

		add(lblPoblacio);
		add(txtPoblacio);
		
		add(lblCP);
		add(txtCP);
		
		add(chkoposicions);
		
		add(pnlBotons);

	}

	@Override
	public void posicionarComponents() {
		GridBagConstraints gbc = new GridBagConstraints();

		//lblDni
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=0;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblDni, gbc);

		//txtDni
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=1;		//	primera columna
		gbc.gridy=0;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=1;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtDni, gbc);
		
		
		
		//btnCercar
		gbc.gridx=2;		//	primera columna
		gbc.gridy=0;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.CENTER;
		gbc.fill= GridBagConstraints.NONE;
		layout.setConstraints(btnCercar, gbc);



		//lblNomUsuari
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=1;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblNomUsuari, gbc);

		//txtNomUsuari
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=1;		//	primera columna
		gbc.gridy=1;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=2;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtNomUsuari, gbc);



		//lblNom
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=2;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblNom, gbc);
		
		//txtNom
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=1;		//	primera columna
		gbc.gridy=2;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=2;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtNom, gbc);
		
		
		//lblCognom
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=3;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblCognoms, gbc);
		
		//txtCognoms
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=1;		//	primera columna
		gbc.gridy=3;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=2;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtCognoms, gbc);
		
		
		
		//lblDataNaixement
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=4;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblDataNaixement, gbc);

		//txtDataNaixement
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=1;		//	primera columna
		gbc.gridy=4;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=2;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtDataNaixement, gbc);		



		//lblPoblacio
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=5;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblPoblacio, gbc);
		
		//txtPoblacio
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=1;		//	primera columna
		gbc.gridy=5;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=2;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtPoblacio, gbc);
		
		

		//lblCP
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=6;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblCP, gbc);
		
		//txtCP
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=1;		//	primera columna
		gbc.gridy=6;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=2;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtCP, gbc);
		
		
		
		//oposicions
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=7;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(chkoposicions, gbc);
		
		
		
		//pnlBotons
		gbc.gridx=2;		//	primera columna
		gbc.gridy=8;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		//gbc.anchor= GridBagConstraints.EAST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(pnlBotons, gbc);
	}
	private class Controlador implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			Object obj = e.getSource();
			if(obj instanceof JButton) {
				JButton btn = (JButton) obj;
				if(btn.getActionCommand().equals("cercar")) {
					esborrarFormulari();
					if(!(txtDni.getText().isEmpty())) {
						Professor professor=null;
						try {
							professor = Controlador_de_Presentacio.getProfessor(txtDni.getText());
						} catch (gestorDBexception e1) {
							Object[] objOpcions3= {"Acceptar"};
							JOptionPane.showInternalOptionDialog(fep.getContentPane(), //Finestra pare
							"Error SQL:\n"+e1.getMessage(), //Missatge de la finestra
							"Eliminar professor", //Títol de la finestra
							JOptionPane.NO_OPTION, //Posarem sempre això.
							JOptionPane.ERROR_MESSAGE, //Tipus icona
							null, //Si no customitzem la icona. Sinò un ImageIcon
							objOpcions3, //Vector de tipus Object[]
							objOpcions3[0]);//Element per defecte del vector Object[]
						}catch (IndexOutOfBoundsException e2) {
							Object[] objOpcions3= {"Acceptar"};
							JOptionPane.showInternalOptionDialog(fep.getContentPane(), //Finestra pare
							"Aquest professor no existeix.", //Missatge de la finestra
							"Eliminar professor", //Títol de la finestra
							JOptionPane.NO_OPTION, //Posarem sempre això.
							JOptionPane.ERROR_MESSAGE, //Tipus icona
							null, //Si no customitzem la icona. Sinò un ImageIcon
							objOpcions3, //Vector de tipus Object[]
							objOpcions3[0]);//Element per defecte del vector Object[]
							
							txtDni.setText("");
							txtDni.grabFocus();
						}
						if(professor!=null) {
							omplirFormulari(professor);
						}
					}
					
				}else if(btn.getActionCommand().equals("cancelar")) {
					Controlador_de_Presentacio.canviFormulari(new FormulariPerDefecte());
				}else if(btn.getActionCommand().equals("eliminar")) {
					
					Object[] objOpcions3= {"Acceptar","Cancel·lar"};
					int n= JOptionPane.showInternalOptionDialog(fep.getContentPane(), //Finestra pare
					"Segur que vols eliminar el professor?", //Missatge de la finestra
					"Eliminar professor", //Títol de la finestra
					JOptionPane.NO_OPTION, //Posarem sempre això.
					JOptionPane.QUESTION_MESSAGE, //Tipus icona
					null, //Si no customitzem la icona. Sinò un ImageIcon
					objOpcions3, //Vector de tipus Object[]
					objOpcions3[0]);//Element per defecte del vector Object[]
					
					if(n==0) {
						try {
							Controlador_de_Presentacio.removeProfessor(txtDni.getText());
						} catch (gestorDBexception e1) {
							Object[] objOpcions31= {"Acceptar","Cancel·lar"};
							JOptionPane.showInternalOptionDialog(fep.getContentPane(), //Finestra pare
							"Error SQL:\n"+e1.getMessage(), //Missatge de la finestra
							"Eliminar professor", //Títol de la finestra
							JOptionPane.NO_OPTION, //Posarem sempre això.
							JOptionPane.QUESTION_MESSAGE, //Tipus icona
							null, //Si no customitzem la icona. Sinò un ImageIcon
							objOpcions31, //Vector de tipus Object[]
							objOpcions31[0]);//Element per defecte del vector Object[]
						}
						txtDni.setText("");
						esborrarFormulari();
						System.out.println("El professor s'ha esborrat correctament");
					}

					}
				}
			}


		}

		private void esborrarFormulari() {
			txtNomUsuari.setText("");
			txtNom.setText("");
			txtCognoms.setText("");
			txtDataNaixement.setText("");
			txtPoblacio.setText("");
			txtCP.setText("");
			chkoposicions.setSelected(false);
			txtDni.grabFocus();
		}



		private void omplirFormulari(Professor profe) {
			txtNomUsuari.setText(profe.getNomUsuari());
			txtNom.setText(profe.getNom());
			txtCognoms.setText(profe.getCognoms());
			txtDataNaixement.setText(profe.getDataNaixement());
			txtPoblacio.setText(profe.getPoblacio());
			String cp =String.valueOf(profe.getCodiPostal());
			txtCP.setText(cp);
			if(profe.isOposicions()==true) {
				chkoposicions.setSelected(true);
			}else {
				chkoposicions.setSelected(false);
			}
		}
	}







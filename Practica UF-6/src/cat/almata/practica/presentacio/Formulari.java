package cat.almata.practica.presentacio;

//	Ficar el abstract es optatiu

public interface Formulari {
	//	Color de fons, imatges de fons...
	public abstract void inicialitzacions();
	//	Creo els components	
	public abstract void crearComponents();
	//	Fico els components a la finestra
	public void afegirComponents();
	//	Fico els components a certa posicio de la finestra
	public void posicionarComponents();
}

package cat.almata.practica.presentacio;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;

import cat.almata.practica.utils.Util;


public class FormulariPerDefecte extends JInternalFrame implements Formulari {

	private static final long serialVersionUID = 1L;
	
	private JLabel imatgeFons;
	private GridBagLayout layout;
	
	public FormulariPerDefecte() {
		inicialitzacions();
		crearComponents();
		afegirComponents();
		posicionarComponents();
		setVisible(true);
	}
	
	@Override
	public void inicialitzacions() {
		getContentPane().setLayout(layout=new GridBagLayout());
		Util.treureBarraTitolInteralFrame(this);

	}

	@Override
	public void crearComponents() {
		imatgeFons=new JLabel(Util.redimensionarImatge("imatges/usuari.jpg", Aplicacio.With, Aplicacio.Height));

	}

	@Override
	public void afegirComponents() {
		add(imatgeFons);

	}

	@Override
	public void posicionarComponents() {
		GridBagConstraints gbc = new GridBagConstraints();

		//	Imatge Fons
		gbc.gridx=0;		//	primera columna
		gbc.gridy=0;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=1;		//	factor de creixement
		gbc.weighty=1;		//	factor de creixement
		gbc.fill= GridBagConstraints.BOTH;
		layout.setConstraints(imatgeFons, gbc);

	}

}

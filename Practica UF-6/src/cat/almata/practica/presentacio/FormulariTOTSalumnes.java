package cat.almata.practica.presentacio;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


import cat.almata.practica.domini.Alumne;
import cat.almata.practica.excepcions.gestorDBexception;





public class FormulariTOTSalumnes extends JInternalFrame implements Formulari {
	private static final long serialVersionUID = 1L;
	
	private GridBagLayout layout;
	private Controlador controlador;
	
	private JButton btnSortir;
	private JTable taula;
	private JScrollPane jspTaula;
	
	private DefaultTableModel model;
	
	private FormulariTOTSalumnes fta;
	
	public FormulariTOTSalumnes() {
		inicialitzacions();
		crearComponents();
		afegirComponents();
		posicionarComponents();
	}
	
	
	@Override
	public void inicialitzacions() {
		setTitle("Formulari consulta tots els alumnes");
		getContentPane().setLayout(layout=new GridBagLayout());
		controlador=new Controlador();
		this.setVisible(true);
		fta=this;
	}

	@Override
	public void crearComponents() {
		btnSortir=new JButton("Sortir");
		btnSortir.addActionListener(controlador);
		btnSortir.setActionCommand("sortir");
		
		
		
		model = new DefaultTableModel();
		model.addColumn("DNI");
		model.addColumn("Nom_Usuari");
		model.addColumn("Data de naixement");
		model.addColumn("DNI Professor");
		
		
		
		taula = new JTable(model);
		
		
		
		jspTaula=new JScrollPane(taula);
		
		omplirTaula();
	}
	
	private void omplirTaula() {
		Map<String, Alumne> alumnes=null;
		try {
			alumnes = Controlador_de_Presentacio.getAllAlumnes();
		} catch (gestorDBexception e) {
			Object[] objOpcions3= {"Acceptar"};
			JOptionPane.showInternalOptionDialog(fta.getContentPane(), //Finestra pare
			"Error SQL:\n"+e.getMessage(), //Missatge de la finestra
			"Modificacio Alumne", //Títol de la finestra
			JOptionPane.NO_OPTION, //Posarem sempre això.
			JOptionPane.ERROR_MESSAGE, //Tipus icona
			null, //Si no customitzem la icona. Sinò un ImageIcon
			objOpcions3, //Vector de tipus Object[]
			objOpcions3[0]);//Element per defecte del vector Object[]
		}
		for(Entry<String, Alumne> objecteMapa: alumnes.entrySet()) {
			Alumne alumne = objecteMapa.getValue();
			Object[] objFila=new Object[4];
			objFila[0]=alumne.getDni();
			objFila[1]=alumne.getNomUsuari();
			objFila[2]=alumne.getDataNaixement();
			objFila[3]=alumne.getDniProfessor();
			model.addRow(objFila);
		}
	}
	
	
	
	
	
	
	

	@Override
	public void afegirComponents() {
		add(btnSortir);
		add(jspTaula);
	}

	@Override
	public void posicionarComponents() {
		GridBagConstraints gbc = new GridBagConstraints();
		
		//jspTaula
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=0;		//	primera fila
		gbc.gridheight=5;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=1;		//	factor de creixement
		gbc.weighty=1;		//	factor de creixement
	//	gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.BOTH;
		layout.setConstraints(jspTaula, gbc);
		
		
		
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=6;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
	//	gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.NONE;
		layout.setConstraints(btnSortir, gbc);
	}
	private class Controlador implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			Object obj = e.getSource();
			if (obj instanceof JButton) {
				JButton boto = (JButton) obj;
				if(boto.getActionCommand().equals("sortir")) {
					Controlador_de_Presentacio.canviFormulari(new FormulariPerDefecte());
				}
			} 
		}
		
	}
}

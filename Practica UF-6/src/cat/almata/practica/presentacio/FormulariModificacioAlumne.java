package cat.almata.practica.presentacio;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.JButton;

import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.github.lgooddatepicker.components.DatePicker;

import cat.almata.practica.domini.Alumne;
import cat.almata.practica.domini.Professor;
import cat.almata.practica.excepcions.gestorDBexception;
import cat.almata.practica.utils.Fonts;
import cat.almata.practica.utils.Util;





public class FormulariModificacioAlumne extends JInternalFrame implements Formulari {

	private static final long serialVersionUID = 1L;

	private GridBagLayout layout;
	private Controlador controlador;
	
	private FormulariModificacioAlumne fma;
	private Professor professor=null;
	

	private JLabel lblProfessor;
	private JTextField txtProfessor;

	private JLabel lblAdjunt;

	private JLabel lblDni;
	private JTextField txtDni;

	private JLabel lblNomUsuari;
	private JTextField txtNomUsuari;

	private JLabel lblDataNaixement;
	private DatePicker txtDataNaixement;

	private JLabel lblPreuMatricula;
	private JTextField txtPreuMatricula;

	private JLabel lblActivitatsEscolars;
	private JRadioButton rbdNatacio;
	private JRadioButton rbdEscalada;
	private JRadioButton rbdEquitacio;
	private JRadioButton rbdSenderisme;
	private JRadioButton rbdEsqui;

	private JLabel lblDescripcio;
	private JTextArea txtDescripcio;

	private JButton btnCercar;
	private JButton btnCancelar;
	private JButton btnAcceptar;
	private JPanel pnlBotons;
	
	





	public FormulariModificacioAlumne() {
		inicialitzacions();
		crearComponents();
		afegirComponents();
		posicionarComponents();
	}

	@Override
	public void inicialitzacions() {
		setTitle("Formulari modificacio dades de l'alumne");
		getContentPane().setLayout(layout=new GridBagLayout());
		Util.treureBarraTitolInteralFrame(this);
		controlador=new Controlador();
		this.setVisible(true);
		fma=this;
	}

	@Override
	public void crearComponents() {
		lblProfessor = new JLabel("DNI Professor:");
		lblProfessor.setFont(Fonts.fontLabel());

		txtProfessor=new JTextField();
		txtProfessor.setFont(Fonts.fontTextField());
		txtProfessor.setEditable(false);



		lblAdjunt=new JLabel("Dades Alumne.");


		lblDni=new JLabel("DNI:");
		lblDni.setFont(Fonts.fontLabel());

		txtDni=new JTextField();
		txtDni.setFont(Fonts.fontTextField());



		lblNomUsuari=new JLabel("Nom de Usuari:");
		lblNomUsuari.setFont(Fonts.fontLabel());

		txtNomUsuari=new JTextField();
		txtNomUsuari.setFont(Fonts.fontTextField());
		txtNomUsuari.setEditable(false);



		lblDataNaixement=new JLabel("Data de Naixement:");
		lblDataNaixement.setFont(Fonts.fontLabel());

		txtDataNaixement= new DatePicker();
		txtDataNaixement.setEnabled(false);



		lblPreuMatricula = new JLabel("Preu Matricula:");
		lblPreuMatricula.setFont(Fonts.fontLabel());

		txtPreuMatricula=new JTextField();
		txtPreuMatricula.setFont(Fonts.fontTextField());
		txtPreuMatricula.setEditable(false);



		lblActivitatsEscolars=new JLabel("Activitats Escolars:");
		lblActivitatsEscolars.setFont(Fonts.fontLabel());

		rbdNatacio=new JRadioButton("Natacio");
		rbdSenderisme=new JRadioButton("Senderisme");
		rbdEquitacio=new JRadioButton("Equitacio");
		rbdEscalada=new JRadioButton("Escalada");
		rbdEsqui=new JRadioButton("Esqui");

		rbdNatacio.setEnabled(false);
		rbdSenderisme.setEnabled(false);
		rbdEquitacio.setEnabled(false);
		rbdEscalada.setEnabled(false);
		rbdEsqui.setEnabled(false);
		
		
		
		lblDescripcio=new JLabel("Descripcio:");
		lblDescripcio.setFont(Fonts.fontLabel());

		txtDescripcio=new JTextArea();
		txtDescripcio.setEditable(false);



		btnCercar=new JButton("Cercar...");
		btnCercar.addActionListener(controlador);
		btnCercar.setActionCommand("cercar");
		btnCancelar=new JButton("Cancelar");
		btnCancelar.addActionListener(controlador);
		btnCancelar.setActionCommand("cancelar");
		btnAcceptar=new JButton("Acceptar");
		btnAcceptar.addActionListener(controlador);
		btnAcceptar.setActionCommand("acceptar");

		pnlBotons=new JPanel();
		pnlBotons.setLayout(new GridLayout(1, 2, 5, 0));	
		pnlBotons.add(btnAcceptar);
		pnlBotons.add(btnCancelar);
		
		

	}

	@Override
	public void afegirComponents() {
		add(lblProfessor);
		add(txtProfessor);

		add(lblAdjunt);

		add(lblDni);
		add(txtDni);
		
		add(btnCercar);

		add(lblNomUsuari);
		add(txtNomUsuari);

		add(lblDataNaixement);
		add(txtDataNaixement);

		add(lblPreuMatricula);
		add(txtPreuMatricula);

		add(lblActivitatsEscolars);
		add(rbdNatacio);
		add(rbdSenderisme);
		add(rbdEquitacio);
		add(rbdEscalada);
		add(rbdEsqui);

		add(lblDescripcio);
		add(txtDescripcio);

		add(pnlBotons);
	}

	@Override
	public void posicionarComponents() {
		GridBagConstraints gbc = new GridBagConstraints();

		//lblProfessor
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=0;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblProfessor, gbc);

		//txtProfessor
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=1;		//	primera columna
		gbc.gridy=0;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=2;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtProfessor, gbc);



		//lblAdjunt
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=1;		//	primera columna
		gbc.gridy=1;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.CENTER;
		gbc.fill= GridBagConstraints.BOTH;
		layout.setConstraints(lblAdjunt, gbc);



		//lblDni
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=2;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblDni, gbc);

		//txtDni
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=1;		//	primera columna
		gbc.gridy=2;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtDni, gbc);


		
		//btnCercar
		gbc.gridx=2;		//	primera columna
		gbc.gridy=2;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.CENTER;
		gbc.fill= GridBagConstraints.NONE;
		layout.setConstraints(btnCercar, gbc);
		
		
		
		//lblNomUsuari
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=3;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblNomUsuari, gbc);

		//txtNomUsuari
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=1;		//	primera columna
		gbc.gridy=3;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=2;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtNomUsuari, gbc);



		//lblDataNaixement
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=4;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblDataNaixement, gbc);

		//txtDataNaixement
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=1;		//	primera columna
		gbc.gridy=4;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=2;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtDataNaixement, gbc);		



		//lblPreuMatricula
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=5;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblPreuMatricula, gbc);

		//txtPreuMatricula
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=1;		//	primera columna
		gbc.gridy=5;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=2;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtPreuMatricula, gbc);



		//lblActivitats
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=6;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblActivitatsEscolars, gbc);

		//rbdCaminar
		gbc.gridx=1;		//	primera columna
		gbc.gridy=6;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(rbdNatacio, gbc);

		//rbdCiclisme
		gbc.gridx=2;		//	primera columna
		gbc.gridy=6;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(rbdSenderisme, gbc);

		//rbdCorrer
		gbc.gridx=0;		//	primera columna
		gbc.gridy=7;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(rbdEquitacio, gbc);

		//rbdEscalada
		gbc.gridx=1;		//	primera columna
		gbc.gridy=7;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(rbdEscalada, gbc);

		//rbdEsqui
		gbc.gridx=2;		//	primera columna
		gbc.gridy=7;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(rbdEsqui, gbc);



		//lblDescripcio
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=8;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblDescripcio, gbc);

		//txtDescripcio
		gbc.gridx=0;		//	primera columna
		gbc.gridy=9;		//	primera fila
		gbc.gridheight=3;	//	alçada
		gbc.gridwidth=3;	//	llargada
		gbc.weightx=1;		//	factor de creixement
		gbc.weighty=1;		//	factor de creixement
		//gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.BOTH;
		layout.setConstraints(txtDescripcio, gbc);



		//panellBotons
		gbc.gridx=2;		//	primera columna
		gbc.gridy=12;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		//gbc.anchor= GridBagConstraints.EAST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(pnlBotons, gbc);

	}
	private class Controlador implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			Object obj = e.getSource();
			if(obj instanceof JButton) {
				JButton btn = (JButton) obj;
				if(btn.getActionCommand().equals("cercar")) {
					txtProfessor.setText("");
					txtNomUsuari.setText("");
					txtDataNaixement.setText("");
					txtPreuMatricula.setText("");
					rbdNatacio.setSelected(false);
					rbdEscalada.setSelected(false);
					rbdEquitacio.setSelected(false);
					rbdSenderisme.setSelected(false);
					rbdEsqui.setSelected(false);
					txtDescripcio.setText("");
					txtDni.grabFocus();
					if(!(txtDni.getText().isEmpty())) {
						Alumne alumne=null;

							try {
								alumne = Controlador_de_Presentacio.getAlumne(txtDni.getText());
							} catch (gestorDBexception e1) {
								Object[] objOpcions3= {"Acceptar"};
								JOptionPane.showInternalOptionDialog(fma.getContentPane(), //Finestra pare
								"Error SQL:\n"+e1.getMessage(), //Missatge de la finestra
								"Modificacio Alumne", //Títol de la finestra
								JOptionPane.NO_OPTION, //Posarem sempre això.
								JOptionPane.ERROR_MESSAGE, //Tipus icona
								null, //Si no customitzem la icona. Sinò un ImageIcon
								objOpcions3, //Vector de tipus Object[]
								objOpcions3[0]);//Element per defecte del vector Object[]
							}catch (IndexOutOfBoundsException e1) {
								Object[] objOpcions3= {"Acceptar"};
								JOptionPane.showInternalOptionDialog(fma.getContentPane(), //Finestra pare
								"Aquest alumne no existeix.", //Missatge de la finestra
								"Modificacio Alumne", //Títol de la finestra
								JOptionPane.NO_OPTION, //Posarem sempre això.
								JOptionPane.ERROR_MESSAGE, //Tipus icona
								null, //Si no customitzem la icona. Sinò un ImageIcon
								objOpcions3, //Vector de tipus Object[]
								objOpcions3[0]);//Element per defecte del vector Object[]
								
								txtDni.setText("");
								txtDni.grabFocus();
							}

						if(alumne!=null) {
							omplir_formulari(alumne);
							activarFormulari(true);
							txtDni.setEditable(false);
						}
					}

				}else if(btn.getActionCommand().equals("cancelar")) {
					Controlador_de_Presentacio.canviFormulari(new FormulariPerDefecte());
				}else if(btn.getActionCommand().equals("acceptar")) {
					Map<String, Professor> professorsTOTS=null;
					try {
						professorsTOTS = Controlador_de_Presentacio.getAllProfessors();
					} catch (gestorDBexception e1) {
						Object[] objOpcions3= {"Acceptar"};
						JOptionPane.showInternalOptionDialog(fma.getContentPane(), //Finestra pare
						"Error SQL:\n"+e1.getMessage(), //Missatge de la finestra
						"Modificacio Alumne", //Títol de la finestra
						JOptionPane.NO_OPTION, //Posarem sempre això.
						JOptionPane.ERROR_MESSAGE, //Tipus icona
						null, //Si no customitzem la icona. Sinò un ImageIcon
						objOpcions3, //Vector de tipus Object[]
						objOpcions3[0]);//Element per defecte del vector Object[]
					}
					boolean a= false;
					for(Professor p: professorsTOTS.values()) { 
						if(p.getDni().equals(txtProfessor.getText())) a=true;
					}
					if(a==false) {
						Object[] objOpcions3= {"Acceptar"};
						JOptionPane.showInternalOptionDialog(fma.getContentPane(), //Finestra pare
						"Aquest professor no existeix.\nFica'n un que existeixi o creal.", //Missatge de la finestra
						"Modificacio Alumne", //Títol de la finestra
						JOptionPane.NO_OPTION, //Posarem sempre això.
						JOptionPane.ERROR_MESSAGE, //Tipus icona
						null, //Si no customitzem la icona. Sinò un ImageIcon
						objOpcions3, //Vector de tipus Object[]
						objOpcions3[0]);//Element per defecte del vector Object[]
					}else {
						boolean esqui=false;
						boolean natacio=false;
						boolean escalada=false;
						boolean equitacio=false;
						boolean senderisme=false;
						
						if(rbdEsqui.isSelected()) {
							esqui=true;
						}
						if(rbdNatacio.isSelected()) {
							natacio=true;
						}
						if(rbdEscalada.isSelected()) {
							escalada=true;
						}
						if(rbdEquitacio.isSelected()) {
							equitacio=true;
						}
						if(rbdSenderisme.isSelected()) {
							senderisme=true;
						}
						
						
						for(Professor p: professorsTOTS.values()) {
							if(p.getDni().equals(txtProfessor.getText())) {
								professor=p;
							}
						}
						
						
						try {
							Controlador_de_Presentacio.removeAlumne(txtDni.getText());
							
							Controlador_de_Presentacio.addAlumne(txtDni.getText(),
									txtNomUsuari.getText(), txtDataNaixement.getText(),
									txtDescripcio.getText(),
									Double.parseDouble(txtPreuMatricula.getText()),
									esqui, natacio, escalada, equitacio,
									senderisme, professor);
						} catch (NumberFormatException e1) {
							Object[] objOpcions3= {"Acceptar"};
							JOptionPane.showInternalOptionDialog(fma.getContentPane(), //Finestra pare
							"El preu de la matricula te que ser numeric", //Missatge de la finestra
							"Modificacio Alumne", //Títol de la finestra
							JOptionPane.NO_OPTION, //Posarem sempre això.
							JOptionPane.ERROR_MESSAGE, //Tipus icona
							null, //Si no customitzem la icona. Sinò un ImageIcon
							objOpcions3, //Vector de tipus Object[]
							objOpcions3[0]);//Element per defecte del vector Object[]
						} catch (gestorDBexception e1) {
							Object[] objOpcions3= {"Acceptar"};
							JOptionPane.showInternalOptionDialog(fma.getContentPane(), //Finestra pare
							"Error SQL:\n"+e1.getMessage(), //Missatge de la finestra
							"Modificacio Alumne", //Títol de la finestra
							JOptionPane.NO_OPTION, //Posarem sempre això.
							JOptionPane.ERROR_MESSAGE, //Tipus icona
							null, //Si no customitzem la icona. Sinò un ImageIcon
							objOpcions3, //Vector de tipus Object[]
							objOpcions3[0]);//Element per defecte del vector Object[]
						}
						esborrarFormulari();
						activarFormulari(false);
						System.out.println("Modificacio alumne");
					}
					
					
					
					
					
					
					



				}

			}

		}
		
		private void omplir_formulari(Alumne alum) {
			txtProfessor.setText(alum.getDniProfessor());
			txtNomUsuari.setText(alum.getNomUsuari());
			txtDataNaixement.setText(alum.getDataNaixement());
			String preu = String.valueOf(alum.getPreuMatricula());
			txtPreuMatricula.setText(preu);
			if(alum.isNatacio()==true) {
				rbdNatacio.setSelected(true);
			}
			if(alum.isEscalada()==true) {
				rbdEscalada.setSelected(true);
			}
			if(alum.isEquitacio()==true) {
				rbdEquitacio.setSelected(true);
			}
			if(alum.isSenderisme()==true) {
				rbdSenderisme.setSelected(true);
			}
			if(alum.isEsqui()==true) {
				rbdEsqui.setSelected(true);
			}
			txtDescripcio.setText(alum.getDescripcio());
		}
		
		private void activarFormulari(boolean activar) {
			txtProfessor.setEditable(activar);
			txtNomUsuari.setEditable(activar);
			txtDataNaixement.setEnabled(activar);
			txtPreuMatricula.setEditable(activar);
			rbdNatacio.setEnabled(activar);
			rbdEscalada.setEnabled(activar);
			rbdEquitacio.setEnabled(activar);
			rbdSenderisme.setEnabled(activar);
			rbdEsqui.setEnabled(activar);
			txtDescripcio.setEditable(activar);
		}
		
		private void esborrarFormulari() {
			txtProfessor.setText("");
			txtDni.setText("");
			txtNomUsuari.setText("");
			txtDataNaixement.setText("");
			txtPreuMatricula.setText("");
			rbdNatacio.setSelected(false);
			rbdEscalada.setSelected(false);
			rbdEquitacio.setSelected(false);
			rbdSenderisme.setSelected(false);
			rbdEsqui.setSelected(false);
			txtDescripcio.setText("");
			txtDni.grabFocus();
			txtDni.setEditable(true);
		}

	}
}





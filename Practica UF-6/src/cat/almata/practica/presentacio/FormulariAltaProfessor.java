package cat.almata.practica.presentacio;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.github.lgooddatepicker.components.DatePicker;

import cat.almata.practica.domini.Professor;
import cat.almata.practica.excepcions.gestorDBexception;
import cat.almata.practica.utils.Fonts;
import cat.almata.practica.utils.Util;





public class FormulariAltaProfessor extends JInternalFrame implements Formulari {

	private static final long serialVersionUID = 1L;

	private GridBagLayout layout;
	private Controlador controlador;

	private FormulariAltaProfessor fap;


	private JLabel lblDni;
	private JTextField txtDni;

	private JLabel lblNomUsuari;
	private JTextField txtNomUsuari;

	private JLabel lblNom;
	private JTextField txtNom;

	private JLabel lblCognoms;
	private JTextField txtCognoms;

	private JLabel lblDataNaixement;
	private DatePicker txtDataNaixement;

	private JLabel lblPoblacio;
	private JTextField txtPoblacio;

	private JLabel lblCP;
	private JTextField txtCP;

	private JCheckBox chkoposicions;

	private JButton btnAcceptar;
	private JButton btnCancelar;
	private JPanel pnlBotons;

	



	public FormulariAltaProfessor() {
		inicialitzacions();
		crearComponents();
		afegirComponents();
		posicionarComponents();
	}

	@Override
	public void inicialitzacions() {
		setTitle("Formulari entrada dades del professor");
		getContentPane().setLayout(layout=new GridBagLayout());
		Util.treureBarraTitolInteralFrame(this);
		controlador=new Controlador();
		this.setVisible(true);
		fap=this;
	}

	@Override
	public void crearComponents() {
		lblDni=new JLabel("DNI:");
		lblDni.setFont(Fonts.fontLabel());

		txtDni=new JTextField();
		txtDni.setFont(Fonts.fontTextField());



		lblNomUsuari=new JLabel("Nom de Usuari:");
		lblNomUsuari.setFont(Fonts.fontLabel());

		txtNomUsuari=new JTextField();
		txtNomUsuari.setFont(Fonts.fontTextField());



		lblNom= new JLabel("Nom:");
		lblNom.setFont(Fonts.fontLabel());

		txtNom=new JTextField();
		txtNom.setFont(Fonts.fontTextField());



		lblCognoms= new JLabel("Cognoms:");
		lblCognoms.setFont(Fonts.fontLabel());

		txtCognoms=new JTextField();
		txtCognoms.setFont(Fonts.fontTextField());



		lblDataNaixement=new JLabel("Data de Naixement:");
		lblDataNaixement.setFont(Fonts.fontLabel());

		txtDataNaixement=new DatePicker();



		lblPoblacio= new JLabel("Poblacio:");
		lblPoblacio.setFont(Fonts.fontLabel());

		txtPoblacio= new JTextField();
		txtPoblacio.setFont(Fonts.fontTextField());



		lblCP=new JLabel("Codi Postal:");
		lblCP.setFont(Fonts.fontLabel());

		txtCP=new JTextField();
		txtCP.setFont(Fonts.fontTextField());


		chkoposicions=new JCheckBox("Oposicions");

		btnAcceptar=new JButton("Acceptar");
		btnAcceptar.addActionListener(controlador);
		btnAcceptar.setActionCommand("acceptar");
		btnCancelar=new JButton("Cancelar");
		btnCancelar.addActionListener(controlador);
		btnCancelar.setActionCommand("cancelar");


		pnlBotons=new JPanel();
		pnlBotons.setLayout(new GridLayout(1, 2, 5, 0));
		pnlBotons.add(btnAcceptar);
		pnlBotons.add(btnCancelar);

	}

	@Override
	public void afegirComponents() {
		add(lblDni);
		add(txtDni);

		add(lblNomUsuari);
		add(txtNomUsuari);

		add(lblNom);
		add(txtNom);

		add(lblCognoms);
		add(txtCognoms);

		add(lblDataNaixement);
		add(txtDataNaixement);

		add(lblPoblacio);
		add(txtPoblacio);

		add(lblCP);
		add(txtCP);

		add(chkoposicions);

		add(pnlBotons);
	}

	@Override
	public void posicionarComponents() {
		GridBagConstraints gbc = new GridBagConstraints();

		//lblDni
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=0;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblDni, gbc);

		//txtDni
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=1;		//	primera columna
		gbc.gridy=0;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=2;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtDni, gbc);



		//lblNomUsuari
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=1;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblNomUsuari, gbc);

		//txtNomUsuari
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=1;		//	primera columna
		gbc.gridy=1;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=2;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtNomUsuari, gbc);



		//lblNom
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=2;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblNom, gbc);

		//txtNom
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=1;		//	primera columna
		gbc.gridy=2;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=2;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtNom, gbc);


		//lblCognom
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=3;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblCognoms, gbc);

		//txtCognoms
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=1;		//	primera columna
		gbc.gridy=3;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=2;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtCognoms, gbc);



		//lblDataNaixement
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=4;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblDataNaixement, gbc);

		//txtDataNaixement
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=1;		//	primera columna
		gbc.gridy=4;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=2;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtDataNaixement, gbc);		



		//lblPoblacio
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=5;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblPoblacio, gbc);

		//txtPoblacio
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=1;		//	primera columna
		gbc.gridy=5;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=2;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtPoblacio, gbc);



		//lblCP
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=6;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(lblCP, gbc);

		//txtCP
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=1;		//	primera columna
		gbc.gridy=6;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=2;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(txtCP, gbc);



		//oposicions
		gbc.insets=new Insets(5, 5, 5, 5);
		gbc.gridx=0;		//	primera columna
		gbc.gridy=7;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=1;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.WEST;
		gbc.fill= GridBagConstraints.HORIZONTAL;
		layout.setConstraints(chkoposicions, gbc);



		//pnlBotons
		gbc.gridx=1;		//	primera columna
		gbc.gridy=8;		//	primera fila
		gbc.gridheight=1;	//	alçada
		gbc.gridwidth=2;	//	llargada
		gbc.weightx=0;		//	factor de creixement
		gbc.weighty=0;		//	factor de creixement
		gbc.anchor= GridBagConstraints.EAST;
		gbc.fill= GridBagConstraints.NONE;
		layout.setConstraints(pnlBotons, gbc);
	}
	private class Controlador implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			Object obj = e.getSource();
			if(obj instanceof JButton) {
				JButton btn = (JButton) obj;
				if(btn.getActionCommand().equals("acceptar")) {


					if(txtDni.getText().isEmpty()) {
						Object[] objOpcions3= {"Acceptar"};
						JOptionPane.showInternalOptionDialog(fap.getContentPane(), //Finestra pare
								"No has ficat el dni.\nEs obligat ficar-lo.", //Missatge de la finestra
								"Alta professor", //Títol de la finestra
								JOptionPane.NO_OPTION, //Posarem sempre això.
								JOptionPane.INFORMATION_MESSAGE, //Tipus icona
								null, //Si no customitzem la icona. Sinò un ImageIcon
								objOpcions3, //Vector de tipus Object[]
								objOpcions3[0]);//Element per defecte del vector Object[]
					}else{

						boolean b=false;
						Map<String, Professor> professorsTOTS=null;
						try {
							professorsTOTS = Controlador_de_Presentacio.getAllProfessors();
						} catch (gestorDBexception e1) {
							Object[] objOpcions3= {"Acceptar"};
							JOptionPane.showInternalOptionDialog(fap.getContentPane(), //Finestra pare
							"Error SQL"+e1.getMessage(), //Missatge de la finestra
							"Alta professor", //Títol de la finestra
							JOptionPane.NO_OPTION, //Posarem sempre això.
							JOptionPane.ERROR_MESSAGE, //Tipus icona
							null, //Si no customitzem la icona. Sinò un ImageIcon
							objOpcions3, //Vector de tipus Object[]
							objOpcions3[0]);//Element per defecte del vector Object[]
						}
						for(Professor c : professorsTOTS.values()) {
							if(c.getDni().equals(txtDni.getText())) {
								b=true;
							}
						}
						if(b==true) {
							Object[] objOpcions3= {"Acceptar"};
							JOptionPane.showInternalOptionDialog(fap.getContentPane(), //Finestra pare
									"Aquest dni ja l'has ficat. Canvial.", //Missatge de la finestra
									"Alta professor", //Títol de la finestra
									JOptionPane.NO_OPTION, //Posarem sempre això.
									JOptionPane.ERROR_MESSAGE, //Tipus icona
									null, //Si no customitzem la icona. Sinò un ImageIcon
									objOpcions3, //Vector de tipus Object[]
									objOpcions3[0]);//Element per defecte del vector Object[]
						}else {
							boolean oposicions=false;





							if(chkoposicions.isSelected()) {
								oposicions=true;
							}

							try {
								Controlador_de_Presentacio.addProfessor(txtDni.getText(),
										txtNomUsuari.getText(),
										txtNom.getText(),
										txtCognoms.getText(),
										txtDataNaixement.getText(),
										txtPoblacio.getText(),
										Integer.valueOf(txtCP.getText()),
										oposicions);
							} catch (NumberFormatException e1) {
								Object[] objOpcions3= {"Acceptar"};
								JOptionPane.showInternalOptionDialog(fap.getContentPane(), //Finestra pare
										"Error SINTAXYS"+e1.getMessage(), //Missatge de la finestra
										"Alta professor", //Títol de la finestra
										JOptionPane.NO_OPTION, //Posarem sempre això.
										JOptionPane.ERROR_MESSAGE, //Tipus icona
										null, //Si no customitzem la icona. Sinò un ImageIcon
										objOpcions3, //Vector de tipus Object[]
										objOpcions3[0]);//Element per defecte del vector Object[]
							} catch (gestorDBexception e1) {
								Object[] objOpcions3= {"Acceptar"};
								JOptionPane.showInternalOptionDialog(fap.getContentPane(), //Finestra pare
										"Error SQL"+e1.getMessage(), //Missatge de la finestra
										"Alta professor", //Títol de la finestra
										JOptionPane.NO_OPTION, //Posarem sempre això.
										JOptionPane.ERROR_MESSAGE, //Tipus icona
										null, //Si no customitzem la icona. Sinò un ImageIcon
										objOpcions3, //Vector de tipus Object[]
										objOpcions3[0]);//Element per defecte del vector Object[]
							}

							esborrarFormulari();

							System.out.println("Alta professor");
						}















					}



				}else if(btn.getActionCommand().equals("cancelar")) {
					Controlador_de_Presentacio.canviFormulari(new FormulariPerDefecte());
				}
			}


		}
		private void esborrarFormulari() {
			txtDni.setText("");
			txtNomUsuari.setText("");
			txtNom.setText("");
			txtCognoms.setText("");
			txtDataNaixement.setText("");
			txtPoblacio.setText("");
			txtCP.setText("");
			chkoposicions.setSelected(false);
			txtDni.grabFocus();
		}
	}

}





package cat.almata.practica.presentacio;

import java.awt.BorderLayout;
import java.util.Map;

import javax.swing.JInternalFrame;


import cat.almata.practica.bbdd.HelperBBDD;
import cat.almata.practica.domini.Alumne;
import cat.almata.practica.domini.Professor;
import cat.almata.practica.excepcions.gestorDBexception;



public class Controlador_de_Presentacio {
	private static Aplicacio aplicacio=null;
	private static JInternalFrame actual=null;
	
	public static void crearAplicacio() {
		if(aplicacio==null) {
			aplicacio=new Aplicacio();
		}
	}
	
	public static void canviFormulari(JInternalFrame nou) {
		if(actual!=null) 
			actual.dispose();
			aplicacio.getContentPane().add(nou, BorderLayout.CENTER);
			aplicacio.setVisible(true);
			actual=nou;
		
	}
	
	
	
	public static void addAlumne(String dni, String nomUsuari,String dataNaixement, String descripcio,
			double preuMatricula, boolean esqui, boolean natacio, boolean escalada,
			boolean equitacio, boolean senderisme, Professor profe) throws gestorDBexception {
		HelperBBDD.addAlumne(new Alumne(dni, nomUsuari, dataNaixement, descripcio, preuMatricula, esqui, natacio, escalada, equitacio, senderisme, profe));
	}
	public static Alumne getAlumne(String keyDNI) throws gestorDBexception  {
		return HelperBBDD.getAlumne(keyDNI);
	}
	public static Map<String, Alumne> getAllAlumnes() throws gestorDBexception{
		return HelperBBDD.getAllAlumnes();
	}
	
	
	
	
	
	public static void addProfessor(String dni, String nomUsuari,
			String nom, String cognoms, String dataNaixement,
			String poblacio, int codiPostal, boolean oposicions) throws gestorDBexception {
		HelperBBDD.addProfessor(new Professor(dni, nomUsuari, nom, cognoms, dataNaixement, poblacio, codiPostal, oposicions));
	}
	public static Professor getProfessor(String keyDNI) throws gestorDBexception {
		return HelperBBDD.getProfessor(keyDNI);
	}
	public static Map<String, Professor> getAllProfessors() throws gestorDBexception{
		return HelperBBDD.getAllProfessors();
	}

	public static void removeAlumne(String keyDNI) throws gestorDBexception {
		HelperBBDD.removeAlumne(keyDNI);
	}

	public static void removeProfessor(String keyDNI) throws gestorDBexception {
		HelperBBDD.removeProfessor(keyDNI);
	}
	
}

package cat.almata.practica;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import cat.almata.practica.bbdd.GestorDB;
import cat.almata.practica.domini.Alumne;
import cat.almata.practica.domini.Professor;
import cat.almata.practica.excepcions.gestorDBexception;

public class Exportar {
	public static void fer() {
		ArrayList<Alumne>alumnes=new ArrayList<Alumne>();
		ArrayList<Professor>professors=new ArrayList<Professor>();
		String textJSONAlumnes;
		String textJSONProfessors;
		BufferedWriter bw=null;
		
		alumnes=obtenirAlumnes();
		professors=obtenirProfessors();
		
		LlistaAlumnes l1 = new LlistaAlumnes(alumnes);
		LlistaProfessors l2 = new LlistaProfessors(professors);
		
		textJSONAlumnes=JSONTools.toJSON(l1);
		textJSONProfessors=JSONTools.toJSON(l2);
		
		try {
			bw = new BufferedWriter(new FileWriter(new File("C:\\Users\\alum-01\\Desktop\\Programacio\\UF-6\\projectes\\Practica UF-6\\JSON\\Alumnes.json")));
			bw.write(textJSONAlumnes);
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			bw = new BufferedWriter(new FileWriter(new File("C:\\Users\\alum-01\\Desktop\\Programacio\\UF-6\\projectes\\Practica UF-6\\JSON\\Professors.json")));
			bw.write(textJSONProfessors);
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	
	
	
	
	private static  ArrayList<Alumne> obtenirAlumnes() {
		String comanda = "SELECT * FROM alumnes";
		ArrayList<Alumne> a=new ArrayList<Alumne>();
		List<Alumne> llistaAlumnes=null;
		try {
			llistaAlumnes=GestorDB.consultaDB(comanda, Alumne.class);
		} catch (gestorDBexception e) {
			e.getMessage();
		}
		
		if(llistaAlumnes!=null) {
			for(Alumne alumne:llistaAlumnes) {
				a.add(alumne);
			}
		}
		return a;
	}




	private static ArrayList<Professor> obtenirProfessors(){
		String comanda = "SELECT * FROM professors";
		ArrayList<Professor> p = new ArrayList<Professor>();
		List<Professor> llistaProfessors=null;
		
		try {
			llistaProfessors=GestorDB.consultaDB(comanda, Professor.class);
		} catch (gestorDBexception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(llistaProfessors!=null) {
			for(Professor profe:llistaProfessors) {
				p.add(profe);
			}
		}
		return p;
	}
}
